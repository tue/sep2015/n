'use strict';
var fs = require('fs');
var gulp = require('gulp');
var server = require('gulp-express');
var spawn = require('child_process').spawn;
var pkg = require('./package.json');

gulp.task('server', function() {
    var options = {};
    // Possible values: false (disable tiny-lr server), number (port), ...
    var livereload = false;

    server.run([pkg.main], options, livereload);
    gulp.watch([pkg.main], ['server']);
});

/** Creates a directory tree if it does not yet exist. */
function makedirs(path) {
    var dirPath = '.';
    path.split('/').forEach(function(dir) {
        dirPath += '/' + dir;
        if (!fs.existsSync(dirPath)) {
            fs.mkdirSync(dirPath);
        }
    });
}

gulp.task('mongod', function() {
    makedirs('db/data');
    // listen on localhost instead of wildcard for security reasons.
    spawn('mongod', [
        '--dbpath', 'db/data',
        '--nounixsocket',
        '--bind_ip', '127.0.0.1',
    ], {
        stdio: 'inherit',
    });
});

// Lazy me, make "gulp" the same as "gulp server" for now.
gulp.task('default', ['server']);
