'use strict';

module.exports = function(app, router) {
    var session = require('../controllers/session');

    router.route('/session')
        .post(session.login)
        .delete(session.logout);
};
