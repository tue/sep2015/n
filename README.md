## Installation
Install dependencies using Python 2.7 because node-gyp is not compatible with
Python 3:

    npm install --python=python2.7

## Start
Simply invoke `gulp server`.
