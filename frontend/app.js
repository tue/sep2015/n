'use strict';

angular.module('someapp', ['ngRoute'])
.config(function($routeProvider) {
    // Sets route definition that will be used on route change when no other
    // route definition is matched.
    // https://docs.angularjs.org/api/ngRoute/provider/$routeProvider#otherwise
    $routeProvider
        .when('/test', {
            templateUrl: 'test.html',
            controller: 'testCtrl',
            controllerAs: 'test'
        })
        .when('/test/:what', {
            templateUrl: 'test.html',
            controller: 'testCtrl',
            controllerAs: 'test'
        });
        //.otherwise('/404');
})
.controller('testCtrl', function($routeParams, $location) {
    var test = this;
    test.who = $routeParams.what || 'a moron';

    test.updateRoute = function() {
        console.log(test.who);
        $location.path('/test/' + (test.who));
    };
});
