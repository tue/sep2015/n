'use strict';

// TODO include local config

/**
 * Server port to listen on.
 */
exports.port = 3000;

/**
 * Mongoose connection URI.
 */
exports.dburi = 'mongodb://localhost/rvs';

/**
 * Secret which signs session ID cookies.
 * https://github.com/expressjs/session#secret
 */
exports.session_secret = 'Very safe. Much secret. Wow.';

/**
 * Path to the static files of the frontend.
 */
exports.frontend_path = '../RVS-frontend';
