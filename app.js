#!/usr/bin/env node
'use strict';
var config = require('./config');
var express = require('express');
var app = express();
var glob = require('glob');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);


// set up database connection
mongoose.connect(config.dburi);
mongoose.connection.on('error', function(err) {
    console.error('Database connection failed', err);
    process.exit(1);
});
//mongoose.set('debug', true);


// persistence (for sessions)
app.use(session({
    secret: config.session_secret,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    name: 'rvssid',
    saveUninitialized: false,
    // Set to false and provide store.touchAfter if you don't want to touch
    // every pageload.
    // https://github.com/kcbanner/connect-mongo#user-content-lazy-session-update
    resave: true,
    // TODO detect whether over SSL?
    // https://github.com/expressjs/session#cookie-options
    //cookie: { secure: true },
}));


// autoload all routes, making it available on a common prefix.
var router = express.Router();
glob.sync('./routes/**/*.js').forEach(function(file) {
    require(file)(app, router);
});
app.use('/api/v1', router);


// set up serving of static frontend files
app.use('/', express.static(config.frontend_path));


var server = app.listen(config.port, function() {
    var host = server.address().address;
    var port = server.address().port;

    if (/:/.test(host)) {
        // IPv6 literals
        host = '[' + host + ']';
    }

    console.log('http://%s:%d', host, port);
});
